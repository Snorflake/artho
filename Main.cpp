﻿/*
   ▄████████    ▄████████     ███        ▄█    █▄     ▄██████▄  
  ███    ███   ███    ███ ▀█████████▄   ███    ███   ███    ███ 
  ███    ███   ███    ███    ▀███▀▀██   ███    ███   ███    ███ 
  ███    ███  ▄███▄▄▄▄██▀     ███   ▀  ▄███▄▄▄▄███▄▄ ███    ███ 
▀███████████ ▀▀███▀▀▀▀▀       ███     ▀▀███▀▀▀▀███▀  ███    ███ 
  ███    ███ ▀███████████     ███       ███    ███   ███    ███ 
  ███    ███   ███    ███     ███       ███    ███   ███    ███ 
  ███    █▀    ███    ███    ▄████▀     ███    █▀     ▀██████▀  
               ███    ███          

Credits:
Herp Derpinstine - A lot of code he helped me with, herp is love, herp is life - imadethem.org
gir489 - A lot of classes from him, drawing, menu and spinbot are the first that come to mind.
Fami - Helped fix my goddamn raytracing.
Vechook - Antileak.
*/


#include "Main.h"

typedef void(__fastcall* InitFn)(CreateInterfaceFn, CreateInterfaceFn, CGlobalVarsBase *);
CInterfacez interfaceManager;
PaintTraverseFn orgPaintTraverse;
CreateMoveFn orgCreateMove;
InitFn orgInit;
INKeyFn orgINKey;
CPTV paintTraverse;
CControlVariables gCvars;
VmtHook gVmt;
CMenu gMenu;
CNetVars netVars;
CAimbot gAimbot;
CCM createMove;
DWORD lel;

using namespace std;

void ShowConsole()
{
	BOOL f1 = AllocConsole();
	HWND hwnd = GetConsoleWindow();
	if (hwnd != NULL)
	{
		HMENU hMenu = GetSystemMenu(hwnd, FALSE);
		if (hMenu != NULL)
		{
			BOOL f2 = DeleteMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
		}
	}
	BOOL f3 = SetConsoleTitle("[Tiskerbox]: Developer Console");
	FILE *t1 = freopen("CONIN$", "r", stdin);
	FILE *t2 = freopen("CONOUT$", "w", stdout);
	FILE *t3 = freopen("CONOUT$", "w", stderr);
}

void CloseConsole()
{
	HWND hwnd = GetConsoleWindow();
	if (hwnd != NULL)
	{
		BOOL f1 = FreeConsole();
		BOOL f2 = PostMessageA(hwnd, WM_QUIT, 0, 0);
	}
}

int getOffsetToVariable(char *szClassName, char *szVariable)
{
	int outputInt = NULL;
	bool breakLoop = false;
	for (ClientClass* clientClass = interfaceManager.Client->GetAllClasses(); clientClass; clientClass->m_pNext)
	{
		if (breakLoop)
			break;
		RecvTable *recvTable = clientClass->m_pRecvTable;
		/*Data table*/
		if (!strcmp(recvTable->GetName(), szClassName ))
		{
			cout << "Table name: " << recvTable->GetName() << endl;
			RecvProp* prop[3];
			for (int i = 0; i < recvTable->GetNumProps(); ++i)
			{
				prop[0] = recvTable->GetPropA(i);
				if (isdigit(prop[0]->m_pVarName[0]))
					continue;
				/*Level 1/3*/
				if (!strcmp(prop[0]->m_pVarName, szVariable))
				{
					breakLoop = true;
					outputInt = prop[0]->GetOffset();
					cout << "Found offset!\n";
				}
				else
				{
					if (!prop[0]->m_pDataTable)
						continue;
					for (int i = 0; i < prop[0]->m_pDataTable->m_nProps; ++i)
					{
						prop[1] = prop[0]->m_pDataTable->GetPropA(i);
						if (isdigit(prop[1]->m_pVarName[0]))
							continue;
						/*Level 2/3*/
						if (!strcmp(prop[1]->m_pVarName, szVariable))
						{
							breakLoop = true;
							outputInt = prop[1]->GetOffset();
						}
						else
						{
							if (!prop[1]->m_pDataTable)
								continue;
							for (int i = 0; i < prop[1]->m_pDataTable->m_nProps; ++i)
							{
								prop[2] = prop[1]->m_pDataTable->GetPropA(i);
								if (isdigit(prop[2]->m_pVarName[0]))
									continue;
								/*Level 3/3*/
								if (!strcmp(prop[2]->m_pVarName, szVariable))
								{
									breakLoop = true;
									outputInt = prop[2]->GetOffset();
								}
							}
						}
					}
				}
			}
			


		}
	}
	char buffer[1024];
	sprintf(buffer, "%x", outputInt);
	cout << "Found " << szClassName << "-->" << szVariable << " [" << buffer << "]\n";
	return outputInt;
}
/*From Herp Derpinstine*/
bool bDataCompare(PBYTE pData, PBYTE bMask, const char* szMask)
{
	for (; *szMask; ++szMask, ++pData, ++bMask)
	if (*szMask == 'x' && *pData !=* bMask)
		return false;
	return (*szMask) == NULL;
}
DWORD sigScan(DWORD handle, DWORD scanSize, PBYTE pattern, char* mask)
{
	for (DWORD i = NULL; i < scanSize; i++)
	if (bDataCompare((BYTE*)(handle + i), pattern, mask))
		return (DWORD)(handle + i);
	return 0;
}
void getNetVars()
{
	try{
		netVars.m_hActiveWeapon = 0xDB0;
		netVars.m_bReadyToBackstab = 0xBB0;
		netVars.m_iHealth = 0xA8;
		netVars.m_fFlags = 0x37C;
		netVars.m_Local = 0xE14;
		//netVars.m_Local = sigScan((DWORD)GetModuleHandleA("client.dll"), 0x8b1000, (PBYTE)"\x6A\x00\x68\x00\x00\x00\x00\x68\x00\x00\x00\x00\x68\x00\x00\x00\x00\xE8\x00\x00\x00\x00\x68\x00\x00\x00\x00\x6A\x00\x6A\x04\x68\x00\x00\x00\x00", "xxx????x????x????x????x????xxxxx????") + 0x7;
		//char buffer[1024];
		//sprintf(buffer, "0x%x", netVars.m_Local);
		//cout << buffer << endl;
		netVars.m_vecViewOffsetx = 0xFC;
		netVars.m_vecViewOffsety = 0x100;
		netVars.m_vecViewOffsetz = 0x104;
		netVars.m_lifeState = 0xA5;

		netVars.m_iTeamNum = 0xB0;

		netVars.m_iPlayerClass = 0xAE8;

		netVars.m_bHasSapper = 0xF2C;
		netVars.m_iUpgradeLevel = 0xEF8;
		netVars.m_iUpgradeMetal = 0xF00;
		netVars.m_nPlayerCond = 0x5AC;
	}
	catch (...){
		cout << "FAILED GETNETVARS\n";
	}
	

	

}
int __fastcall hkINKey(void* _this, void* EDX, int eventcode, ButtonCode_t keynum, const char* pszCurrentBinding)
{
	if (eventcode == 1)
	{
		if (keynum == KEY_INSERT) //insert
		{
			gCvars.menuActive = !gCvars.menuActive;
		}
		if (gCvars.menuActive)
		{
			if (keynum == KEY_UP) // Up
			{
				if (gMenu.iMenuIndex > 0) gMenu.iMenuIndex--;
				else gMenu.iMenuIndex = gMenu.iMenuItems - 1;
				return 0;
			}
			else if (keynum == KEY_DOWN) // Down
			{
				if (gMenu.iMenuIndex < gMenu.iMenuItems - 1) gMenu.iMenuIndex++;
				else gMenu.iMenuIndex = 0;
				return 0;
			}
			else if (keynum == KEY_LEFT) // Left
			{
				if (gMenu.pMenu[gMenu.iMenuIndex].value)
				{
					gMenu.pMenu[gMenu.iMenuIndex].value[0] -= gMenu.pMenu[gMenu.iMenuIndex].flStep;
					if (gMenu.pMenu[gMenu.iMenuIndex].value[0] < gMenu.pMenu[gMenu.iMenuIndex].flMin)
						gMenu.pMenu[gMenu.iMenuIndex].value[0] = gMenu.pMenu[gMenu.iMenuIndex].flMax;
				}
				return 0;
			}
			else if (keynum == KEY_RIGHT) // Right
			{
				if (gMenu.pMenu[gMenu.iMenuIndex].value)
				{
					gMenu.pMenu[gMenu.iMenuIndex].value[0] += gMenu.pMenu[gMenu.iMenuIndex].flStep;
					if (gMenu.pMenu[gMenu.iMenuIndex].value[0] > gMenu.pMenu[gMenu.iMenuIndex].flMax)
						gMenu.pMenu[gMenu.iMenuIndex].value[0] = gMenu.pMenu[gMenu.iMenuIndex].flMin;
				}
				return 0;
			}
		}
	}
	return orgINKey(_this, EDX, eventcode, keynum, pszCurrentBinding);

}
void __fastcall hkPaintTraverse(void* thisptr, void* EDX, vgui::VPANEL vguiPanel, bool forceRepaint, bool allowForce)
{
	orgPaintTraverse(thisptr, EDX, vguiPanel, forceRepaint, allowForce);
	paintTraverse.Invoke(vguiPanel);
}
//Unused because detected
CUserCmd* __stdcall hkGetUserCmd(int sequence_number)
{
	static CUserCmd* pCommands = *(CUserCmd **)((DWORD)interfaceManager.Input + 0xC4);
	CUserCmd *pCmd = &pCommands[sequence_number % MULTIPLAYER_BACKUP];
	if (pCmd != NULL)
	{
		pCmd->command_number = 2076615043;
		pCmd->random_seed = 39;
	}
	return pCmd;
}
void __fastcall hkCreateMove(void* thisptr, void* EDX, int i, float f, bool b)
{
	orgCreateMove(thisptr, EDX, i, f, b);
	void* _ebp = NULL;
	_asm{
		mov _ebp, ebp;
	};
		createMove.Invoke(i, _ebp);
}
void InitThread()
{
	cAL *gAL = new cAL;
	if (!gAL->Verified()){
		_asm {
			mov eax, 0xDEADBEEF;
			call eax;
		}
	}
	ShowConsole();
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, 4 + 0 * 16);
	cout << "Verified user!\n";
	/*INTERFACES*/
	interfaceManager.GetInterfaces();
	getNetVars();
	/*for (ClientClass* pClass = interfaceManager.Client->GetAllClasses(); pClass; pClass = pClass->m_pNext)
	{
		const char* name = pClass->GetName();
	}*/
	/*EVENT HOOKS*/
	//createMove.WriteLog("Pre-Listener!");
	//interfaceManager.GameEventManager->AddListener(&listener, "player_death", false);
	//createMove.WriteLog("Listener added!");
	/*HOOKS*/
		//orgPaintTraverse = (PaintTraverseFn)panelHook->dwHookMethod((DWORD)hkPaintTraverse, 41);
	orgPaintTraverse = (PaintTraverseFn)gVmt.hookvmt((DWORD)interfaceManager.VPanel, (DWORD)hkPaintTraverse, 41);
	orgCreateMove = (CreateMoveFn)gVmt.hookvmt((DWORD)interfaceManager.Client, (DWORD)hkCreateMove, 21);
	orgINKey = (INKeyFn)gVmt.hookvmt((DWORD)interfaceManager.Client, (DWORD)hkINKey, 20);
	gDrawManager.Initialize();
	gMenu.Initialize();
	/*FINISHED*/
	cout << "[Tiskerbox] Injected without error!\n";
}
bool APIENTRY DllMain(HINSTANCE hInstance, DWORD CallReason, LPVOID lpReserved)
{
	if (CallReason == DLL_PROCESS_ATTACH)
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)InitThread, 0, 0, 0);
	if (CallReason == DLL_PROCESS_DETACH)
		CloseConsole();
	return true;
}
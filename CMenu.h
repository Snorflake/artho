#pragma once
#include "Main.h"
#include "Vars.h"
typedef struct CMenuItems_t
{
	char szTitle[30];
	float* value;
	float flMin;
	float flMax;
	float flStep;
};
/*Stole this from Darkstorm*/
class CMenu
{
public:
	bool bMenuActive;
	int iMenuIndex;
	int iMenuItems;
	CMenuItems_t pMenu[120];
	int AddItem(int nIndex, char szTitle[128], float* value, float flMin, float flMax, float flStep)
	{
		strcpy(pMenu[nIndex].szTitle, szTitle);
		pMenu[nIndex].value = value;
		pMenu[nIndex].flMin = flMin;
		pMenu[nIndex].flMax = flMax;
		pMenu[nIndex].flStep = flStep;
		return (nIndex + 1);
	}

	void RGB_DWORD(DWORD &dwColor, BYTE r, BYTE g, BYTE b)
	{
		dwColor = (r << 24) | (g << 16) | (b << 8);
	}

	DWORD dwWhite = NULL;
	DWORD dwGreen = NULL;

	void Initialize(void)
	{
		RGB_DWORD(dwWhite, 255, 255, 255);
		RGB_DWORD(dwGreen, 0, 255, 0);
		int i = 0;
		i = AddItem(i, "- ESP", &gCvars.esp, 0, 1, 1);
		i = AddItem(i, " - - Box ESP", &gCvars.esp_box, 0, 1, 1);
		i = AddItem(i, " - - Name ESP", &gCvars.esp_name, 0, 1, 1);
		i = AddItem(i, " - - Health ESP", &gCvars.esp_health, 0, 1, 1);
		i = AddItem(i, " - - Distance ESP", &gCvars.esp_dist, 0, 1, 1);
		i = AddItem(i, " - - Draw Team", &gCvars.drawteam, 0, 1, 1);
		//i = AddItem(i, " - Class ESP", &gCvars.esp_class, 0, 1, 1);
		i = AddItem(i, " - Aimbot", &gCvars.aimbot, 0, 1, 1);
		i = AddItem(i, " - - Silent", &gCvars.aimbot_silent, 0, 1, 1);
		i = AddItem(i, " - - AutoShoot", &gCvars.aimbot_auto_shoot, 0, 1, 1);
		i = AddItem(i, " - - Smoothing", &gCvars.aimbot_smooth, 0, 1, 1);
		i = AddItem(i, " - Dodgeball", &gCvars.blast, 0, 1, 1);
		i = AddItem(i, " - Triggerbot", &gCvars.triggerbot, 0, 1, 1);
		//if (gCvars.triggerbot)
		i = AddItem(i, " - - Triggerbot HSONLY", &gCvars.triggerbotHS, 0, 1, 1);
		i = AddItem(i, " - Auto Backstab", &gCvars.auto_bs, 0, 1, 1);
		i = AddItem(i, " - Spinbot", &gCvars.spinbot, 0, 1, 1);
		i = AddItem(i, " - Bunny Hop", &gCvars.bhop, 0, 1, 1);
		//i = AddItem(i, " - Auto Strafe", &gCvars.bhop_strafe, 0, 1, 1);
		i = AddItem(i, " -  - Crouchjump", &gCvars.bhop_crouch, 0, 1, 1);
		i = AddItem(i, " - Crosshair", &gCvars.crosshair, 0, 4, 1);
		i = AddItem(i, " - Achievement Spam", &gCvars.achspam, 0, 1, 1);
		i = AddItem(i, " - Namestealer", &gCvars.namesteal, 0, 1, 1);
		i = AddItem(i, " - Fakelag", &gCvars.fakelag, 0, 1, 1);

		iMenuItems = i;
		gCvars.misc_menu_x = 300;
		gCvars.misc_menu_y = 75;
		gCvars.misc_menu_w = 200;
	}

	void DrawMenu(void)
	{
		int x = gCvars.misc_menu_x,
			xx = x + 155,
			y = gCvars.misc_menu_y,
			w = gCvars.misc_menu_w,
			h = gDrawManager.GetESPHeight();

		gDrawManager.DrawRect(x, y - (h + 4), w, iMenuItems * 11 + 21, COLORCODE(30, 30, 30, 128));
		gDrawManager.OutlineRect(x, y - (h + 4), w, (h + 4), COLORCODE(30, 30, 30, 128));
		gDrawManager.OutlineRect(x, y - (h + 4), w, iMenuItems * 11 + 21, COLORCODE(0, 128, 255, 255));

		gDrawManager.DrawString(false, true, x + 12, y - (h + 2), COLORCODE(0, 128, 255, 255),"Tiskerbox");

		for (int i = 0; i < iMenuItems; i++)
		{
			if (i != iMenuIndex)
			{
				gDrawManager.DrawString(false, true, x + 2, y + (11 * i), COLORCODE(255, 255, 255, 255), pMenu[i].szTitle);
				gDrawManager.DrawString(false, true, xx, y + (11 * i), COLORCODE(255, 255, 255, 255), "%2.2f", pMenu[i].value[0]);
			}
			else
			{
				gDrawManager.DrawRect(x + 1, y + (11 * i), w - 2, h, COLORCODE(255, 255, 255, 80));
				gDrawManager.DrawString(false, true, x + 2, y + (11 * i), COLORCODE(0, 128, 255, 255), pMenu[i].szTitle);
				gDrawManager.DrawString(false, true, xx, y + (11 * i), COLORCODE(0, 128, 255, 255), "%2.2f", pMenu[i].value[0]);
			}
		}
	}
};
extern CMenu gMenu;
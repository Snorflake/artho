#pragma once
#include "Main.h"
#include "in_buttons.h"
#include "NetVars.h"
#include "util_shared.h"
#include "Spread Helper.h"
#define XASSERT( x ) if( !x ) MessageBoxW( 0, L#x, 0, 0 );
#define streql(x,y) Q_strcmp( (x) , (y) ) == 0;

class CTraceFilterCustom : public ITraceFilter // credits to Casual_Hacker
{
public:
	bool ShouldHitEntity(CBaseEntity* pEnt, int contentsMask)
	{
		if (pEnt == interfaceManager.ClientEntList->GetClientEntity(interfaceManager.Engine->GetLocalPlayer()))
			return false;

		if (!strcmp(pEnt->GetClientClass()->GetName(), "CTFPlayer"))
			return true;

		if (!strcmp(pEnt->GetClientClass()->GetName(), "CFuncRespawnRoomVisualizer") || !strcmp(pEnt->GetClientClass()->GetName(), "CTFMedigunShield") ||
			!strcmp(pEnt->GetClientClass()->GetName(), "CFuncAreaPortalWindow")) // TO DO: Add string hashes
			return false;

		return true;
	}
	TraceType_t GetTraceType() const { return TRACE_EVERYTHING; }
};
class CListener : public IGameEventListener2
{
public:
	~CListener()
	{
		interfaceManager.GameEventManager->RemoveListener(this);
	};
	void FireGameEvent(IGameEvent *evt);

};
extern CListener listener;
class CCM
{
public:
	int tickCount = 0;
	int tickCountNS = 0;
	int oldMouseX = 0;
	int oldMouseY = 0;
	void Invoke(int i, void* _ebp)
	{
		if (gCvars.fakelag)
		UTIL_FakeLag(_ebp);
		CUserCmd* pCmd = interfaceManager.Input->GetUserCmd(i);
		CBaseEntity* me = (CBaseEntity*)interfaceManager.ClientEntList->GetClientEntity(interfaceManager.Engine->GetLocalPlayer());
		int baseWeaponIndex = *MakePtr(int*, me, netVars.m_hActiveWeapon);
		C_BaseCombatWeapon *baseWeapon = (C_BaseCombatWeapon *)interfaceManager.ClientEntList->GetClientEntityFromHandle(baseWeaponIndex);
		int flags = *(int*)((DWORD)me + netVars.m_fFlags);
		/*Bunny hop*/
		if (gCvars.bhop)
		{

			if (pCmd->buttons & 1 << 1) {
				//Broken
				if (gCvars.bhop_strafe)
				{

					int ix = NULL, iy = NULL;
					float ox = NULL, oy = NULL;
					LPPOINT point = NULL;
					point->x = ix;
					point->y = iy;
					GetCursorPos(point);
					ScreenToClient(GetForegroundWindow(), point);
						ox = (point->x + oldMouseX) * .5f;
						oy = (point->y + oldMouseY) * .5f;
						oldMouseX = point->x;
						oldMouseY = point->y;
						if (ox < 0) // SPINNING RIGHT
							pCmd->sidemove = -10000;
						else if (ox > 0) // SPINNING LEFT
							pCmd->sidemove = 10000;
					

				}
				if (!(flags&FL_ONGROUND) && !(flags&FL_PARTIALGROUND))
					pCmd->buttons &= ~1 << 1;
				else
					pCmd->buttons |= 1 << 1;
			}

		}
		if (gCvars.bhop_crouch && pCmd->buttons & 1<<1)
		{
			pCmd->buttons |= 1 << 2;
		}		
		if (gCvars.triggerbot && canHitPlayer(pCmd))
			pCmd->buttons |= 1 << 0;
		if (gCvars.achspam)
		{
			if (tickCount > 66 * 2)
				tickCount = 0;
			if (tickCount == 0)
			{
				KeyValues *kv = new KeyValues("AchievementEarned");
				kv->SetInt("achievementID", 1733);
				interfaceManager.Engine->ServerCmdKeyValues(kv);
				cout << "Achievement sent!" << endl;
			}
			tickCount++;

		}

		if (gCvars.namesteal)
		{
			if (tickCountNS > 66 * 35)
				tickCountNS = 0;
			if (tickCountNS == 0)
			{
				player_info_t info;
				srand(time(0));
				int index = rand() % interfaceManager.Engine->GetMaxClients() + 1;
				if (interfaceManager.Engine->GetPlayerInfo(index, &info))
				{

					nameSteal(index);

				}
			}


		}
		

		if (gCvars.aimbot)
			gAimbot.Aimbot(pCmd, _ebp);
		if (gCvars.auto_bs)
		{
			if (baseWeapon)
			{
				if (!strcmp(baseWeapon->GetClientClass()->GetName(), "CTFKnife"))
				{
					WriteLog("Knife equipped");
					if (*MakePtr(bool*, baseWeapon, netVars.m_bReadyToBackstab)){
						pCmd->buttons |= 1 << 0;
					}
				}

			}

		}
		if (gCvars.blast)
			autoBlast(pCmd);

		/*Keep spinbot at the bottom*/
		if (gCvars.spinbot)
			Spinbot(pCmd);

		/*Verification*/
		CInput::CVerifiedUserCmd *verifiedCommands = interfaceManager.Input->m_pVerifiedCommands;
		CInput::CVerifiedUserCmd *verified = &verifiedCommands[i % MULTIPLAYER_BACKUP];
		if (verified)
		{
			verified->m_cmd = *pCmd;
			verified->m_crc = pCmd->GetChecksum();
		}
	}
	void WriteLog(const char* string)
	{
		FILE* file = fopen("SUPERSEXYLOGFILE.txt", "a");
		fprintf(file, "%s\n", string);
		fclose(file);
	}
	void nameSteal(int index)
	{
		CBasePlayer* target = (CBasePlayer*)interfaceManager.ClientEntList->GetClientEntity(index);
		if (!target)
			return;
		player_info_t info;
		if (interfaceManager.Engine->GetPlayerInfo(index, &info))
		{
			//Credits Herp Derpstine for character.
			stringstream newname;
			newname << info.name << (static_cast<char>(32)) << (static_cast<char>(31));
			setName(newname.str().c_str());
		}
	}


	void UTIL_FakeLag(void *__ebp){
		byte *sendPacket = (byte *)(*(char **)__ebp - 0x1);

		static int queued = 0;
		if (queued >= 0)
		{
			queued++;

			// queue up 2 cmds
			if ((queued < 2) /*&& !backup_firedLastTick*/)
			{
				*sendPacket = 1;
			}
			else
			{
				// queue up cmd*
				*sendPacket = 0;
			}
		}
		else
		{
			// queue up cmd
			*sendPacket = 1;
		}

		// finalize warp and reset
		if ((queued == 15) /*|| backup_firedLastTick*/)
		{
			queued = 0;
		}
	};
	private:
		void autoBlast(CUserCmd* pCmd)
		{
			CBaseEntity* me = (CBaseEntity*)interfaceManager.ClientEntList->GetClientEntity(interfaceManager.Engine->GetLocalPlayer());
			if (!me)
				return;
			Vector viewoffset = Vector(*(float*)((DWORD)me + netVars.m_vecViewOffsetx), *(float*)((DWORD)me + netVars.m_vecViewOffsety), *(float*)((DWORD)me + netVars.m_vecViewOffsetz));
			Vector eyepos = me->GetAbsOrigin() + viewoffset;
			QAngle outangle;
			for (int i = 0; i < interfaceManager.ClientEntList->GetMaxEntities(); i++)
			{
				CBaseEntity *entity = (CBaseEntity *)interfaceManager.ClientEntList->GetClientEntity(i);
				if (!entity)
					continue;

				if (!strcmp(entity->GetClientClass()->GetName(), "CTFProjectile_Rocket"))
				{
					Vector rocketpos = entity->GetAbsOrigin();
					int team = *MakePtr(int*, entity, netVars.m_iTeamNum);
					int myteam = *MakePtr(int*, me, netVars.m_iTeamNum);
					if (myteam != team)
					{
						float distance = (int)floor(flGetDistance(me->GetAbsOrigin(), entity->GetAbsOrigin()) + 0.5);
						if (distance < 150)
						{

							Vector outpos = Vector(rocketpos.x - eyepos.x, rocketpos.y - eyepos.y, rocketpos.z - eyepos.z);
							VectorAngles(outpos, outangle);
							//gAimbot.FixMovement(pCmd, outangle);
							pCmd->viewangles = outangle;
							interfaceManager.Engine->SetViewAngles(outangle);
							pCmd->buttons |= IN_ATTACK2;
						}
					}
				}
			}
		}
		float flGetDistance(Vector from, Vector to)
		{
			Vector angle;
			angle.x = to.x - from.x;	angle.y = to.y - from.y;	angle.z = to.z - from.z;

			return sqrt(angle.x*angle.x + angle.y*angle.y + angle.z*angle.z);
		}
		C_BaseCombatWeapon * GetBaseCombatActiveWeapon(C_BaseEntity* pEntity)
		{
			C_BaseCombatWeapon* hResult = NULL;
			__asm
			{
				MOV  EDI, pEntity;
				MOV  EAX, DWORD PTR DS : [EDI];
				MOV  ECX, EDI;
				CALL DWORD PTR DS : [EAX + 0x300]
					MOV  hResult, EAX;
			}
			return hResult;
		}
		//===================================================================================
		int iGetWeaponID(C_BaseCombatWeapon* pWeapon)
		{
			int iWeaponID;
			_asm
			{
				MOV ESI, pWeapon;
				MOV EDX, DWORD PTR DS : [ESI];
				MOV EAX, DWORD PTR DS : [EDX + 0x51C];
				MOV ECX, ESI;
				CALL EAX;
				MOV iWeaponID, EAX;
			}
			return iWeaponID;
		}
		/*Stole this from Darkstorm*/
		void Spinbot(CUserCmd* pCommand)
		{
			if (pCommand->buttons & 1 << 0)
				return;

			Vector viewforward, viewright, viewup, aimforward, aimright, aimup;
			QAngle qAimAngles;

			float forward = pCommand->forwardmove;
			float right = pCommand->sidemove;
			float up = pCommand->upmove;

			qAimAngles.Init(0.0f, pCommand->viewangles.y, 0.0f);
			AngleVectors(qAimAngles, &viewforward, &viewright, &viewup);

			float fTime = interfaceManager.Engine->Time();
			pCommand->viewangles.y = (vec_t)(fmod(fTime / 0.1f * 360.0f, 360.0f));
			qAimAngles.Init(0.0f, pCommand->viewangles.y, 0.0f);

			AngleVectors(qAimAngles, &aimforward, &aimright, &aimup);

			Vector vForwardNorm;		Normalize(viewforward, vForwardNorm);
			Vector vRightNorm;			Normalize(viewright, vRightNorm);
			Vector vUpNorm;				Normalize(viewup, vUpNorm);

			pCommand->forwardmove = DotProduct(forward * vForwardNorm, aimforward) + DotProduct(right * vRightNorm, aimforward) + DotProduct(up * vUpNorm, aimforward);
			pCommand->sidemove = DotProduct(forward * vForwardNorm, aimright) + DotProduct(right * vRightNorm, aimright) + DotProduct(up * vUpNorm, aimright);
			pCommand->upmove = DotProduct(forward * vForwardNorm, aimup) + DotProduct(right * vRightNorm, aimup) + DotProduct(up * vUpNorm, aimup);
		}
		void Normalize(Vector &vIn, Vector &vOut)
		{
			float flLen = vIn.Length();

			if (flLen == 0)
			{
				vOut.Init(0, 0, 1);
				return;
			}

			flLen = 1 / flLen;

			vOut.Init(vIn.x * flLen, vIn.y * flLen, vIn.z * flLen);
		}

		inline bool canHitPlayer(CUserCmd* pCmd)
		{
			Ray_t ray; 
			trace_t trace; 
			CTraceFilter filter;
			C_BasePlayer* me = (C_BasePlayer*)interfaceManager.ClientEntList->GetClientEntity(interfaceManager.Engine->GetLocalPlayer());
			if (!me)
				return false;
			filter.AddClass("CFuncRespawnRoomVisualizer");
			filter.AddClass("CFuncAreaPortalWindow");
			Vector angles;
			Vector viewoffset = Vector(*(float*)((DWORD)me  + netVars.m_vecViewOffsetx), *(float*)((DWORD)me  + netVars.m_vecViewOffsety), *(float*)((DWORD)me + netVars.m_vecViewOffsetz));
			Vector eyepos = me->GetAbsOrigin() + viewoffset;
			AngleVectors(pCmd->viewangles, &angles);
			angles = angles * 8192 + eyepos;

			ray.Init(eyepos, angles);
			interfaceManager.EngineTrace->TraceRay(ray, 0x46004003, NULL, &trace);

			C_BaseEntity* pEntity = trace.m_pEnt;
			if (!pEntity)
				return false;
			player_info_t info;
			if (!interfaceManager.Engine->GetPlayerInfo(trace.m_pEnt->entindex(), &info))
				return false;

			if (*(int*)((DWORD)me + netVars.m_iTeamNum) == *(int*)((DWORD)trace.m_pEnt + netVars.m_iTeamNum))
				return false;
			if (gCvars.triggerbotHS)
			{
				if (trace.hitgroup == HITGROUP_HEAD)
					return true;
				return false;
			}

			return true;
		}
		void setName(const char* name)
		{
			static ConVar *namecvar = interfaceManager.Cvar->FindVar("name");
			if (namecvar)
			{
				FnChangeCallback_t namecvarcallback = namecvar->m_fnChangeCallback;
				namecvar->m_fnChangeCallback = NULL;
				namecvar->SetValue(name);
				namecvar->InternalSetValue(name);
				namecvar->m_fnChangeCallback = namecvarcallback;
			}
		}

};
extern CCM createMove;
#pragma once
#include "Main.h"

class CInterfacez
{
public:

	IMaterialSystem				*Material;
	IClientEntityList			*ClientEntList;
	IVEngineClient				*Engine;
	IFileSystem					*FileSystem;
	ICvar						*Cvar;
	IBaseClientDLL				*Client;
	IClientDLLSharedAppSystems	*ClientInterfaces;
	IPrediction					*Prediction;
	IEngineSound					*Sound;
	IGameEventManager2					*GameEventManager;
	IVModelRender				*ModelRender;
	IVRenderView					*RenderView;
	IEngineTrace					*EngineTrace;
	IEngineVGui					*EngineVGui;
	IVEfx						*Effects;
	IVModelInfoClient			*ModelInfo;
	IVDebugOverlay				*DebugOverlay;
	IStudioRender				*Studio;
	IPhysics						*Physics;
	IPhysicsSurfaceProps			*PhysicsSurfaceProps;
	vgui::ISurface				*Surface;
	vgui::IPanel					*VPanel;
	IUniformRandomStream			*Random;
	CGlobalVarsBase				*Globals;
	CInput						*Input;
	INetChannelInfo				*Net;
	void							*ClientMode;
	void* GetPointer(CreateInterfaceFn Factory, const char *pszInterface)
	{
		char szPossibleInterface[1024];
		void *m_pInterface = NULL;
		m_pInterface = (void*)Factory(szPossibleInterface, 0);
		if (m_pInterface)
			return m_pInterface;
		for (int i = 1; i < 100; i++)
		{
			if (i < 10)
				sprintf(szPossibleInterface, "%s00%i", pszInterface, i);
			if (i > 9)
				sprintf(szPossibleInterface, "%s0%i", pszInterface, i);
			m_pInterface = (void*)Factory(szPossibleInterface, 0);
			if (m_pInterface != NULL)
				break;
		}
		return(m_pInterface);
	}

	void GetInterfaces()
	{
		CreateInterfaceFn					MaterialFactory = NULL;
		CreateInterfaceFn					EngineFactory = NULL;
		CreateInterfaceFn					ClientFactory = NULL;
		CreateInterfaceFn					FileFactory = NULL;
		CreateInterfaceFn					StudioFactory = NULL;
		CreateInterfaceFn					PhysicsFactory = NULL;
		CreateInterfaceFn					VGUIFactory = NULL;
		CreateInterfaceFn					VGUIMatFactory = NULL;
		CreateInterfaceFn					SteamClientFactory = NULL;
		CreateInterfaceFn					InputFactory = NULL;
		CreateInterfaceFn					VStdFactory = NULL;

		ClientFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("client.dll"), "CreateInterface");
		EngineFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("engine.dll"), "CreateInterface");
		PhysicsFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("vphysics.dll"), "CreateInterface");
		StudioFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("StudioRender.dll"), "CreateInterface");
		MaterialFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("MaterialSystem.dll"), "CreateInterface");
		VGUIFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("vguimatsurface.dll"), "CreateInterface");
		VGUIMatFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("vgui2.dll"), "CreateInterface");
		InputFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("inputsystem.dll"), "CreateInterface");
		VStdFactory = (CreateInterfaceFn)GetProcAddress(GetModuleHandleA("vstdlib.dll"), "CreateInterface");

		Material = (IMaterialSystem*)GetPointer(MaterialFactory, "VMaterialSystem");
		Studio = (IStudioRender*)GetPointer(StudioFactory, "VStudioRender");
		Physics = (IPhysics*)GetPointer(PhysicsFactory, "VPhysics");
		PhysicsSurfaceProps = (IPhysicsSurfaceProps*)GetPointer(PhysicsFactory, "VPhysicsSurfaceProps");
		Surface = (ISurface*)GetPointer(VGUIFactory, "VGUI_Surface");
		Engine = (IVEngineClient*)GetPointer(EngineFactory, "VEngineClient");
		Random = (IUniformRandomStream*)GetPointer(EngineFactory, "VEngineRandom");
		Sound = (IEngineSound*)GetPointer(EngineFactory, "IEngineSoundClient");
		GameEventManager = (IGameEventManager2*)GetPointer(EngineFactory, "GAMEEVENTSMANAGER002");
		ModelRender = (IVModelRender*)GetPointer(EngineFactory, "VEngineModel");
		RenderView = (IVRenderView*)GetPointer(EngineFactory, "VEngineRenderView");
		EngineTrace = (IEngineTrace*)GetPointer(EngineFactory, "EngineTraceClient");
		EngineVGui = (IEngineVGui*)GetPointer(EngineFactory, "VEngineVGui");
		Effects = (IVEfx*)GetPointer(EngineFactory, "VEngineEffects");
		ModelInfo = (IVModelInfoClient*)GetPointer(EngineFactory, "VModelInfoClient");
		DebugOverlay = (IVDebugOverlay*)GetPointer(EngineFactory, "VDebugOverlay");
		ClientEntList = (IClientEntityList*)GetPointer(ClientFactory, "VClientEntityList");
		ClientInterfaces = (IClientDLLSharedAppSystems*)GetPointer(ClientFactory, "VClientDllSharedAppSystems");
		Prediction = (IPrediction*)GetPointer(ClientFactory, "VClientEntityList");
		Client = (IBaseClientDLL*)GetPointer(ClientFactory, "VClient");
		VPanel = (IPanel*)GetPointer(VGUIMatFactory, "VGUI_Panel");
		Cvar = (ICvar*)GetPointer(VStdFactory, "VEngineCvar");
		Input = **(CInput***)((*(DWORD**)Client)[14] + 0x2);
	}
};
 extern CInterfacez interfaceManager;

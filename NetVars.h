#pragma once
#include "Main.h"
#ifndef NETVAR
#define NETVAR
struct NetVarData_s
{
	const char* szClassName;
	const char* szVariable;
	int iOffset;
};

class CNetVars
{
public:
	int m_iHealth;
	int m_bHasSapper;
	int m_bReadyToBackstab;
	int m_hActiveWeapon;
	int m_iPlayerClass;
	int m_iTeamNum;
	int m_iUpgradeLevel;
	int m_nPlayerCond;
	int m_iUpgradeMetal;
	int m_lifeState;
	int m_fFlags;
	int m_Local;
	int m_vecViewOffsetx;
	int m_vecViewOffsety;
	int m_vecViewOffsetz;
	bool bInited;
	std::vector<struct NetVarData_s> stdVecDataStorage;
	void Init()
	{
		if (!this->bInited)
		{
			this->getOffset();
			this->bInited = true;
		}
	}

	void getOffset()
	{
		ClientClass *pClass = interfaceManager.Client->GetAllClasses();

		for (; pClass; pClass = pClass->m_pNext)
		{
			RecvTable* pTable = pClass->m_pRecvTable;

			if (pTable->GetNumProps() <= 1)
				continue;

			DumpDataTable(pTable);
		}
	}

	void DumpDataTable(RecvTable *pTable)
	{
		for (int i = 0; i < pTable->GetNumProps(); i++)
		{
			RecvProp* pProp = pTable->GetProp(i);

			if (!pProp)
				continue;

			const char* szClassName = pTable->GetName();
			const char* szVariable = pProp->GetName();
			int iOffset = pProp->GetOffset();

			struct NetVarData_s data;
			{
				data.szClassName = szClassName;
				data.szVariable = szVariable;
				data.iOffset = iOffset;
			}

			this->stdVecDataStorage.push_back(data);

			if (pProp->GetDataTable())
				DumpDataTable(pProp->GetDataTable()); // recursive call.
		}
	}

	int GetNetVar(const char* szClassName, const char* szVariable)
	{
		for (auto it = this->stdVecDataStorage.begin(); it != this->stdVecDataStorage.end(); it++)
		{
			const char* it_szClassName = (*it).szClassName;
			const char* it_szVariable = (*it).szVariable;
			int it_iOffset = (*it).iOffset;
			if (!strcmp(it_szClassName, szClassName) && !strcmp(it_szVariable, szVariable))
			{
				char buffer[1024];
				sprintf(buffer, "0x%x", it_iOffset);
				cout << szVariable << " found! Offset: [" << buffer << "]\n"; 
				return it_iOffset;
			}
				
		}
		return NULL;
	}
}; extern CNetVars netVars;
#endif
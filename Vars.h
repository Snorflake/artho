#pragma once
#include "Main.h"

class CControlVariables
{
public:
	
	float esp_health;
	float esp_dist;
	float esp_name;
	float esp_box;
	float esp_class;
	float misc_menu_x;
	float misc_menu_y;
	float misc_menu_w;
	float spinbot;
	float bhop;
	float bhop_crouch;
	float bhop_strafe;
	float triggerbot;
	float triggerbotHS;
	float auto_bs;
	float crosshair;
	float blast;
	float esp;
	float drawteam;
	float fakelag;
	float achspam;
	float namesteal;
	float aimbot;
	float aimbot_auto_shoot;
	float aimbot_silent;
	float aimbot_smooth;
	bool menuActive;

};
//===================================================================================
extern CControlVariables gCvars;
//===================================================================================
#pragma once
#include "Main.h"
//===================================================================================

CDrawManager gDrawManager;
//CInterfacez interfaceManager;

#define ESP_HEIGHT 10
unsigned long m_Font;
HFont gameFont;
//===================================================================================
void CDrawManager::Initialize()
{
	if (interfaceManager.Surface == NULL)
		return;
	interfaceManager.Engine->GetScreenSize(gScreenWidth, gScreenHeight);
	m_Font = interfaceManager.Surface->CreateFont();
	interfaceManager.Surface->SetFontGlyphSet(m_Font, "04b09", ESP_HEIGHT, 500, 0, 0, 0x200);
	gameFont = 5;
}
//===================================================================================
void CDrawManager::DrawString(bool centred, bool esp, int x, int y, DWORD dwColor, const wchar_t *pszText)
{
	if (pszText == NULL)
		return;
	int w, h;
	interfaceManager.Surface->GetTextSize(esp ? m_Font : gameFont, pszText, w, h);
	if (centred)
	{
		interfaceManager.Surface->DrawSetTextPos(x - w * .5f, y - h * .5f);
	}
	else
	interfaceManager.Surface->DrawSetTextPos(x, y);
	interfaceManager.Surface->DrawSetTextFont(esp ? m_Font : gameFont);
	interfaceManager.Surface->DrawSetTextColor(RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor));

	interfaceManager.Surface->DrawPrintText(pszText, wcslen(pszText));
}
//===================================================================================
void CDrawManager::DrawString(bool centred, bool esp, int x, int y, DWORD dwColor, const char *pszText, ...)
{
	if (pszText == NULL)
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start(va_alist, pszText);
	vsprintf_s(szBuffer, pszText, va_alist);
	va_end(va_alist);

	wsprintfW(szString, L"%S", szBuffer);

	int w, h;
	interfaceManager.Surface->GetTextSize(esp ? m_Font : gameFont, szString, w, h);
	if (centred)
	{
		interfaceManager.Surface->DrawSetTextPos(x - w * .5f, y - h *.5f);
	}
	else
		interfaceManager.Surface->DrawSetTextPos(x, y);
	interfaceManager.Surface->DrawSetTextFont(esp ? m_Font : gameFont);
	interfaceManager.Surface->DrawSetTextColor(RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor));
	interfaceManager.Surface->DrawPrintText(szString, wcslen(szString));
}
//===================================================================================
byte CDrawManager::GetESPHeight()
{
	return ESP_HEIGHT;
}
//===================================================================================
void CDrawManager::DrawRect(int x, int y, int w, int h, DWORD dwColor)
{
	interfaceManager.Surface->DrawSetColor(RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor));
	interfaceManager.Surface->DrawFilledRect(x, y, x + w, y + h);
}
//===================================================================================
void CDrawManager::OutlineRect(int x, int y, int w, int h, DWORD dwColor)
{
	interfaceManager.Surface->DrawSetColor(RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor));
	interfaceManager.Surface->DrawOutlinedRect(x, y, x + w, y + h);
}
//===================================================================================
void CDrawManager::DrawBox(Vector vOrigin, int r, int g, int b, int alpha, int box_width, int radius)
{
	Vector vScreen;

	if (!WorldToScreen(vOrigin, vScreen))
		return;

	int radius2 = radius << 1;

	OutlineRect(vScreen.x - radius + box_width, vScreen.y - radius + box_width, radius2 - box_width, radius2 - box_width, 0x000000FF);
	OutlineRect(vScreen.x - radius - 1, vScreen.y - radius - 1, radius2 + (box_width + 2), radius2 + (box_width + 2), 0x000000FF);
	DrawRect(vScreen.x - radius + box_width, vScreen.y - radius, radius2 - box_width, box_width, COLORCODE(r, g, b, alpha));
	DrawRect(vScreen.x - radius, vScreen.y + radius, radius2, box_width, COLORCODE(r, g, b, alpha));
	DrawRect(vScreen.x - radius, vScreen.y - radius, box_width, radius2, COLORCODE(r, g, b, alpha));
	DrawRect(vScreen.x + radius, vScreen.y - radius, box_width, radius2 + box_width, COLORCODE(r, g, b, alpha));
}
//===================================================================================
bool CDrawManager::WorldToScreen(Vector &vOrigin, Vector &vScreen)
{
	interfaceManager.Engine->GetScreenSize(gScreenWidth, gScreenHeight);
	const VMatrix& worldToScreen = interfaceManager.Engine->WorldToScreenMatrix();
	float w = 0;
	w = w + worldToScreen[3][0] * vOrigin[0];
	w = w + worldToScreen[3][1] * vOrigin[1];
	w = w + worldToScreen[3][2] * vOrigin[2] + worldToScreen[3][3];
	vScreen.z = 0;
	if (w > 0.001)
	{
		float fl1DBw = 1 / w;
		vScreen.x = (gScreenWidth / 2) + (0.5 * ((worldToScreen[0][0] * vOrigin[0] + worldToScreen[0][1] * vOrigin[1] + worldToScreen[0][2] * vOrigin[2] + worldToScreen[0][3]) * fl1DBw) * gScreenWidth + 0.5);
		vScreen.y = (gScreenHeight / 2) - (0.5 * ((worldToScreen[1][0] * vOrigin[0] + worldToScreen[1][1] * vOrigin[1] + worldToScreen[1][2] * vOrigin[2] + worldToScreen[1][3]) * fl1DBw) * gScreenHeight + 0.5);
		return true;
	}
	return false;
}


void CDrawManager::DrawPlayerBox(C_BasePlayer *pEnt, DWORD dwColor)
{
	if (pEnt == NULL)
		return;


	Vector vMon, vNom,vBot,vTop;

	vNom = pEnt->GetAbsOrigin();

	vMon = vNom + Vector(0, 0, 80.f);
	if (WorldToScreen(vNom, vBot) && WorldToScreen(vMon, vTop))
	{
		int iH = (vBot.y - vTop.y);
		int iW = iH / 4.f;
		OutlineRect(vTop.x - iW, vTop.y, iW * 2, iH, dwColor);
	}
}

float CDrawManager::flGetDistance(Vector from, Vector to)
{
	Vector angle;
	angle.x = to.x - from.x;	angle.y = to.y - from.y;	angle.z = to.z - from.z;

	return sqrt(angle.x*angle.x + angle.y*angle.y + angle.z*angle.z);
}
void CDrawManager::DrawCrosshair(int iValue)

{
	int m_iScreenWidth;
	int	m_iScreenHeight;
	interfaceManager.Engine->GetScreenSize(m_iScreenWidth, m_iScreenHeight);
	DWORD dwRed = COLORCODE(255, 50, 0, 255);
	DWORD dwWhite = COLORCODE(255, 255, 255, 255);

	switch (iValue)
	{
	case 1:
		DrawRect((m_iScreenWidth / 2) - 25, m_iScreenHeight / 2, 50, 1, dwWhite);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2) - 25, 1, 50, dwWhite);
		DrawRect((m_iScreenWidth / 2) - 7, m_iScreenHeight / 2, 14, 1, dwRed);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2) - 7, 1, 14, dwRed);
		break;
	case 2:
		DrawRect(m_iScreenWidth / 2 - 14, (m_iScreenHeight / 2), 9, 1, dwWhite);
		DrawRect(m_iScreenWidth / 2 + 5, (m_iScreenHeight / 2), 9, 1, dwWhite);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2) - 14, 1, 9, dwWhite);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2) + 5, 1, 9, dwWhite);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2), 1, 1, dwWhite);

		break;
	case 3:
		DrawRect(m_iScreenWidth / 2 - 14, (m_iScreenHeight / 2), 9, 2, dwWhite);
		DrawRect(m_iScreenWidth / 2 + 6, (m_iScreenHeight / 2), 9, 2, dwWhite);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2) - 14, 2, 9, dwWhite);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2) + 7, 2, 9, dwWhite);
		DrawRect(m_iScreenWidth / 2, (m_iScreenHeight / 2), 2, 2, dwWhite);
		break;
	case 4:
		DrawRect((m_iScreenWidth / 2) - 2, (m_iScreenHeight / 2) - 2, 4, 4, dwWhite);
		break;
	}
}
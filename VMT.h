#pragma once
#include <Windows.h>

class VmtHook
{

public:
	DWORD hookvmt(DWORD dwInterface, DWORD dwHook, int index)
	{
		DWORD* vtable = (DWORD*)*(DWORD*)dwInterface;
		DWORD backup = NULL;
		MEMORY_BASIC_INFORMATION mbi;
		VirtualQuery((LPCVOID)vtable, &mbi, sizeof(mbi));
		VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_READWRITE, &mbi.Protect);
		backup = vtable[index];
		vtable[index] = (DWORD)dwHook;
		VirtualProtect(mbi.BaseAddress, mbi.RegionSize, mbi.Protect, &mbi.Protect);
		cout << "Hooked index: " << index << endl;
		return backup;
	}
};
extern VmtHook gVmt;
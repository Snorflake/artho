#pragma once
#include "Main.h"
#include "in_buttons.h"
typedef enum
{
	TFCond_Slowed = (1 << 0), //Toggled when a player is slowed down. 
	TFCond_Zoomed = (1 << 1), //Toggled when a player is zoomed. 
	TFCond_Disguising = (1 << 2), //Toggled when a Spy is disguising. 
	TFCond_Disguised = (1 << 3), //Toggled when a Spy is disguised. 
	TFCond_Cloaked = (1 << 4), //Toggled when a Spy is invisible. 
	TFCond_Ubercharged = (1 << 5), //Toggled when a player is ‹berCharged. 
	TFCond_TeleportedGlow = (1 << 6), //Toggled when someone leaves a teleporter and has glow beneath their feet. 
	TFCond_Taunting = (1 << 7), //Toggled when a player is taunting. 
	TFCond_UberchargeFading = (1 << 8), //Toggled when the ‹berCharge is fading. 
	TFCond_CloakFlicker = (1 << 9), //Toggled when a Spy is visible during cloak. 
	TFCond_Teleporting = (1 << 10), //Only activates for a brief second when the player is being teleported; not very useful. 
	TFCond_Kritzkrieged = (1 << 11), //Toggled when a player is being crit buffed by the KritzKrieg. 
	TFCond_TmpDamageBonus = (1 << 12), //Unknown what this is for. Name taken from the AlliedModders SDK. 
	TFCond_DeadRingered = (1 << 13), //Toggled when a player is taking reduced damage from the Deadringer. 
	TFCond_Bonked = (1 << 14), //Toggled when a player is under the effects of The Bonk! Atomic Punch. 
	TFCond_Stunned = (1 << 15), //Toggled when a player's speed is reduced from airblast or a Sandman ball. 
	TFCond_Buffed = (1 << 16), //Toggled when a player is within range of an activated Buff Banner. 
	TFCond_Charging = (1 << 17), //Toggled when a Demoman charges with the shield. 
	TFCond_DemoBuff = (1 << 18), //Toggled when a Demoman has heads from the Eyelander. 
	TFCond_CritCola = (1 << 19), //Toggled when the player is under the effect of The Crit-a-Cola. 
	TFCond_InHealRadius = (1 << 20), //Unused condition, name taken from AlliedModders SDK. 
	TFCond_Healing = (1 << 21), //Toggled when someone is being healed by a medic or a dispenser. 
	TFCond_OnFire = (1 << 22), //Toggled when a player is on fire. 
	TFCond_Overhealed = (1 << 23), //Toggled when a player has >100% health. 
	TFCond_Jarated = (1 << 24), //Toggled when a player is hit with a Sniper's Jarate. 
	TFCond_Bleeding = (1 << 25), //Toggled when a player is taking bleeding damage. 
	TFCond_DefenseBuffed = (1 << 26), //Toggled when a player is within range of an activated Battalion's Backup. 
	TFCond_Milked = (1 << 27), //Player was hit with a jar of Mad Milk. 
	TFCond_MegaHeal = (1 << 28), //Player is under the effect of Quick-Fix charge. 
	TFCond_RegenBuffed = (1 << 29), //Toggled when a player is within a Concheror's range. 
	TFCond_MarkedForDeath = (1 << 30), //Player is marked for death by a Fan O'War hit. Effects are similar to TFCond_Jarated. 

	TFCondEx_SpeedBuffAlly = (1 << 0), //Toggled when a player gets hit with the disciplinary action. 
	TFCondEx_HalloweenCritCandy = (1 << 1), //Only for Scream Fortress event maps that drop crit candy. 
	TFCondEx_CritCanteen = (1 << 2), //Player is getting a crit boost from a canteen.
	TFCondEx_CritHype = (1 << 4), //Soda Popper crits. 
	TFCondEx_CritOnFirstBlood = (1 << 5), //Arena first blood crit buff. 
	TFCondEx_CritOnWin = (1 << 6), //End of round crits. 
	TFCondEx_CritOnFlagCapture = (1 << 7), //CTF intelligence capture crits. 
	TFCondEx_CritOnKill = (1 << 8), //Unknown what this is for. 
	TFCondEx_RestrictToMelee = (1 << 9), //Unknown what this is for. 
	TFCondEx_Reprogrammed = (1 << 11), //MvM Bot has been reprogrammed.
	TFCondEx_PyroCrits = (1 << 12), //Player is getting crits from the Mmmph charge. 
	TFCondEx_PyroHeal = (1 << 13), //Player is being healed from the Mmmph charge. 
	TFCondEx_FocusBuff = (1 << 14), //Player is getting a focus buff.
	TFCondEx_DisguisedRemoved = (1 << 15), //Disguised remove from a bot.
	TFCondEx_MarkedForDeathSilent = (1 << 16), //MvM related.
	TFCondEx_DisguisedAsDispenser = (1 << 17), //Bot is disguised as dispenser.
	TFCondEx_Sapped = (1 << 18), //MvM bot is being sapped.
	TFCondEx_UberchargedHidden = (1 << 19), //MvM Related
	TFCondEx_UberchargedCanteen = (1 << 20), //Player is recieveing ubercharge from a canteen.
	TFCondEx_HalloweenBombHead = (1 << 21), //Player has a bomb on their head from Merasmus.
	TFCondEx_HalloweenThriller = (1 << 22), //Players are forced to dance from Merasmus.
}TFCond;
class CAimbot
{
public:
	void FixMovement(CUserCmd *pCommand, const QAngle& va)
	{
		Vector viewforward, viewright, viewup, aimforward, aimright, aimup;
		float forward = pCommand->forwardmove;
		float right = pCommand->sidemove;
		float up = pCommand->upmove;
		AngleVectors(pCommand->viewangles, &viewforward, &viewright, &viewup);
		AngleVectors(va, &aimforward, &aimright, &aimup);
		Vector vForwardNorm = viewforward * (1 / viewforward.Length());
		Vector vRightNorm = viewright * (1 / viewright.Length());
		Vector vUpNorm = viewup * (1 / viewup.Length());
		pCommand->forwardmove = DotProduct(forward  *vForwardNorm, aimforward) + DotProduct(right  *vRightNorm, aimforward) + DotProduct(up * vUpNorm, aimforward);
		pCommand->sidemove = DotProduct(forward *vForwardNorm, aimright) + DotProduct(right  *vRightNorm, aimright) + DotProduct(up * vUpNorm, aimright);
		pCommand->upmove = DotProduct(forward  *vForwardNorm, aimup) + DotProduct(right  *vRightNorm, aimup) + DotProduct(up * vUpNorm, aimup);
	}
	void Aimbot(CUserCmd* pCmd, void *ebp)
	{
		/*Local Player*/
		CBasePlayer* me = (CBasePlayer*)interfaceManager.ClientEntList->GetClientEntity(interfaceManager.Engine->GetLocalPlayer());	
		if (!me)
			return;
		int baseWeaponIndex = *MakePtr(int*, me, netVars.m_hActiveWeapon);
		C_BaseCombatWeapon *baseWeapon = (C_BaseCombatWeapon *)interfaceManager.ClientEntList->GetClientEntityFromHandle(baseWeaponIndex);

		player_info_t localplayer;
		if (!interfaceManager.Engine->GetPlayerInfo(me->entindex(), &localplayer))
			return;
		if (*MakePtr(BYTE*, me, netVars.m_lifeState) == LIFE_ALIVE)
		{
			Vector viewoffset = Vector(*(float*)((DWORD)me + netVars.m_vecViewOffsetx), *(float*)((DWORD)me + netVars.m_vecViewOffsety), *(float*)((DWORD)me + netVars.m_vecViewOffsetz));
			Vector eyepos = me->GetAbsOrigin() + viewoffset;
			QAngle nextViewAngle, finalangle;
			bool locked = false;
			/*Check for this value, its fucking rediculous :^)*/
			for (int index = 0; index <= interfaceManager.ClientEntList->GetMaxEntities(); index++)
			{
				bool breakLoop = false;
				if (index == interfaceManager.Engine->GetLocalPlayer())
					continue;
				CBaseEntity* target = (CBaseEntity*)interfaceManager.ClientEntList->GetClientEntity(index);
				if (!target)
					continue;
				player_info_t info;
				if (!interfaceManager.Engine->GetPlayerInfo(target->entindex(), &info))
					continue;
				if (target->IsDormant())
					continue;
				if (*MakePtr(BYTE*, target, netVars.m_lifeState) != LIFE_ALIVE)
					continue;
				if (*(int*)((DWORD)me + netVars.m_iTeamNum) == *(int*)((DWORD)target + netVars.m_iTeamNum))
					continue;
				if (*MakePtr(int*, target, netVars.m_nPlayerCond) == (TFCond_Bonked || TFCond_DeadRingered || TFCond_Ubercharged))
					continue;
				const model_t *model = target->GetModel();
				if (!model)
					continue;
				matrix3x4_t bones[MAXSTUDIOBONES];
				if (!target->SetupBones(bones, MAXSTUDIOBONES, BONE_USED_BY_HITBOX,0))
					continue;
				if (!bones)
					continue;
				studiohdr_t *hdr = interfaceManager.ModelInfo->GetStudiomodel(model);
				if (!hdr)
					continue;
				mstudiohitboxset_t *hitboxTable = hdr->pHitboxSet(0);
				if (!hitboxTable)
					continue;
				int hitboxnum = hitboxTable->numhitboxes;
				for (int i = 0; i < hitboxnum; i++)
				{
					if (breakLoop)
						continue;
					mstudiobbox_t* currentHitbox = hdr->pHitbox(i, 0);
					if (!currentHitbox)
						continue;
					float eyeanglex, eyeangley;
					QAngle eyeangle;
					Vector hitboxpos, min, max;
					/*Correcting models*/
					eyeanglex = *MakePtr(float*, target, 0x21FC);
					eyeangley = *MakePtr(float*, target, 0x2200);
					eyeangle = QAngle(eyeanglex, eyeangley, 0);
					MatrixAngles(bones[currentHitbox->bone], eyeangle, hitboxpos);
					VectorTransform(currentHitbox->bbmin, bones[currentHitbox->bone], min);
					VectorTransform(currentHitbox->bbmax, bones[currentHitbox->bone], max);
					hitboxpos = (min + max) * 0.5f;
					/*Traceray here*/
					Ray_t ray;
					trace_t trace;
					CTraceFilter filter;
					/*		if (!strcmp(pEnt->GetClientClass()->GetName(), "CFuncRespawnRoomVisualizer") || !strcmp(pEnt->GetClientClass()->GetName(), "CTFMedigunShield") ||
			!strcmp(pEnt->GetClientClass()->GetName(), "CFuncAreaPortalWindow")) // TO DO: Add string hashes*/
					CBasePlayer *targetz = (CBasePlayer *)target;
					filter.AddClass("CFuncRespawnRoomVisualizer");
					filter.AddClass("CTFMedigunShield");
					filter.AddClass("CFuncAreaPortalWindow");
					ray.Init(eyepos, hitboxpos);
					interfaceManager.EngineTrace->TraceRay(ray, MASK_SHOT, &filter, &trace);				
					if (!trace.allsolid && trace.m_pEnt == target && trace.hitgroup == HITGROUP_HEAD)
					{
						Vector outpos = Vector(hitboxpos.x - eyepos.x, hitboxpos.y - eyepos.y, hitboxpos.z - eyepos.z);
					
						Vector evel;
						Vector mvel;
						for (int i = 0; i < 3; i++)
						{
							evel[i] = *MakePtr(float*, target, 0x120 + (i * 0x4));
							mvel[i] = *MakePtr(float*, me, 0x120 + (i * 0x4));
						}

						if (gCvars.aimbot_smooth)
						{
							VectorAngles(outpos, finalangle);							
							nextViewAngle.x = AngleNormalize(nextViewAngle.x);
							nextViewAngle.y = AngleNormalize(nextViewAngle.y);
							Smooth(finalangle, nextViewAngle, pCmd->viewangles, 4);
						}
						else
						{
							VectorAngles(outpos, finalangle);
							nextViewAngle = finalangle;
						}
						locked = true;
						breakLoop = true;
					}
				}				
			}
			if (locked)
			{				
				if (gCvars.aimbot_silent)
				{
					FixMovement(pCmd, nextViewAngle);
					pCmd->viewangles = nextViewAngle;
					if (gCvars.aimbot_auto_shoot)
						pCmd->buttons |= 1 << 0;				
				}
				else
				{
					pCmd->viewangles = nextViewAngle;
					interfaceManager.Engine->SetViewAngles(nextViewAngle);
					if (gCvars.aimbot_auto_shoot)
						pCmd->buttons |= 1 << 0;
				}
			}
		}
	}
	void WriteLog(const char* string)
	{
		FILE* file = fopen("SUPERSEXYLOGFILE.txt", "a");
		fprintf(file, "%s\n", string);
		fclose(file);
	}
	private:
		float vAngleNormalize(float angle)
		{
			angle = fmodf(angle, 360.0f);
			if (angle > 180)
			{
				angle -= 360;
			}
			if (angle < -180)
			{
				angle += 360;
			}
			return angle;
		}
		void Smooth(QAngle& src, QAngle& back, QAngle& flLocalAngles, int iSmooth)
		{
			float smoothdiff[2];
			src.x -= flLocalAngles.x;
			src.y -= flLocalAngles.y;

			if (src.x >= 180)
				src.x -= 360;
			if (src.y > 180)
				src.y -= 360;
			if (src.x < -180)
				src.x += 360;
			if (src.y < -180)
				src.y += 360;

			smoothdiff[0] = src.x / iSmooth;
			smoothdiff[1] = src.y / iSmooth;
			back.x = flLocalAngles.x + smoothdiff[0];
			back.y = flLocalAngles.y + smoothdiff[1];
			back.z = flLocalAngles.z;

			if (back.x > 180)
				back.x -= 360;
			if (back.y > 180)
				back.y -= 360;
			if (back.x < -180)
				back.x += 360;
			if (back.y < -180)
				back.y += 360;
		}
		QAngle LerpAngle(float scale, QAngle oldangle, QAngle newangle)
		{
			QAngle temp;
			temp.x = newangle.x - (oldangle.x * scale);
			temp.y = newangle.y - (oldangle.y * scale);
			temp.z = newangle.z - (oldangle.z * scale);
			return temp;
		}
	
}; extern CAimbot gAimbot;
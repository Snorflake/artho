#pragma once

#define MakePtr( Type, dwBase, dwOffset ) ( ( Type )( DWORD( dwBase ) + (DWORD)( dwOffset ) ) )
#include "SDK.h"
#include "Interfaces.h"
#include "PaintTraverse.h"
#include "CAimbot.h"
#include "CDrawManager.h"
#include "CreateMove.h"
#include "Vars.h"
#include "CMenu.h"
#include "AntiLeak.h"
#include "crc32.h"
#include "Weapon List.h"
#include "NetVars.h"
#include "Spread Helper.h"
#include "VMT.h"
typedef void(__fastcall* PaintTraverseFn)(void*,void*,vgui::VPANEL, bool, bool);
typedef void(__fastcall* CreateMoveFn)(void*, void*, int, float, bool);
typedef int(__fastcall *INKeyFn)(void*, void*, int, ButtonCode_t, const char*);

extern CreateMoveFn orgCreateMove;

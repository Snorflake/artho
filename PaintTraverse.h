#pragma once
#include "Main.h"
#include "CDrawManager.h"
#include "CMenu.h"
#include "Vars.h"
#include "NetVars.h"
typedef enum {
	TFTeam_Unassigned = 0,
	TFTeam_Spectator = 1,
	TFTeam_Red = 2,
	TFTeam_Blue = 3
} TFTeam;
class CPTV
{
public:
	void Invoke(unsigned int panel){
		interfaceManager.Engine->GetScreenSize(gDrawManager.gScreenWidth, gDrawManager.gScreenHeight);
		const char* pszPanel = interfaceManager.VPanel->GetClassName(panel);
		unsigned int mSTP = NULL;
		if (mSTP == NULL)
		{
			if (!strcmp(pszPanel, "MatSystemTopPanel")){
				mSTP = panel;
			}
		}
		if (panel == mSTP && interfaceManager.Engine->IsInGame() && !interfaceManager.Engine->IsTakingScreenshot())
		{
			gDrawManager.DrawString(false, false, 5, 5, 0x00FFFFFF, "Tiskerbox");
			for (int i = 0; i < interfaceManager.Engine->GetMaxClients(); i++)
			{
				if (gCvars.esp)
				DrawESP(i);
			}
			if (gCvars.menuActive)
			{
				gMenu.DrawMenu();
			}
			gDrawManager.DrawCrosshair(gCvars.crosshair);
		}
	}
private:

	DWORD teamColour(int team)
	{
		switch (team)
		{
		case TFTeam_Red :
			return 0xBC4B4BFF;
			break;
		case TFTeam_Blue:
			return 0x0000FFFF;
			break;			
		}
		return 0x00FF00FF;
	}
	float flGetDistance(Vector from, Vector to)
	{
		Vector angle;
		angle.x = to.x - from.x;	
		angle.y = to.y - from.y;	
		angle.z = to.z - from.z;

		return sqrt(angle.x*angle.x + angle.y*angle.y + angle.z*angle.z);
	}
	DWORD hpColour(int hp)
	{
		if (hp > 74)
		{
			return 0x00FF00FF;
		}
		if (hp <75 & hp > 24)
		{
			return 0xFFA700FF;
		}
		if (hp < 25)
		{
			return 0xD62D20FF;
		}
	}
	Vector GetBonePos(int bone, CBaseEntity *entity)
	{
		static QAngle angle;
		Vector bonepos;
		matrix3x4_t matrix[MAXSTUDIOBONES];
		if (entity->SetupBones(matrix, MAXSTUDIOBONES, BONE_USED_BY_HITBOX, 0))
		{
			MatrixAngles(matrix[bone], angle, bonepos);
			return bonepos;
		}
		return bonepos;
	}
	void DrawESP(int iIndex)
	{

		player_info_t playerInfo;
		C_BasePlayer *ePlayer = (C_BasePlayer *)interfaceManager.ClientEntList->GetClientEntity(iIndex);
		C_BaseEntity *eMe = (C_BaseEntity *)interfaceManager.ClientEntList->GetClientEntity(interfaceManager.Engine->GetLocalPlayer());

		if (iIndex == interfaceManager.Engine->GetLocalPlayer())
			return;

		if (!ePlayer)
			return;
		
		if (*(int*)((DWORD)ePlayer + netVars.m_iTeamNum) == *(int*)((DWORD)eMe + netVars.m_iTeamNum) && !gCvars.drawteam)
			return;
		int teamnum = *MakePtr(int*, ePlayer, netVars.m_iTeamNum);
		if (interfaceManager.Engine->GetPlayerInfo(iIndex, &playerInfo) && *MakePtr(BYTE*,ePlayer,netVars.m_lifeState) == LIFE_ALIVE && !ePlayer->IsDormant())
		{
			
			Vector vTop, vBot;
			Vector vMon, vNom;

			vNom = ePlayer->GetAbsOrigin();

			vMon = vNom + Vector(0, 0, 80.f);
			int flags = *MakePtr(int*, ePlayer, netVars.m_fFlags);
			if (flags & 1 << 2)
			{
				vMon = vNom + Vector(0, 0, 40.f);
			}
			gDrawManager.WorldToScreen(vNom, vBot); 
			gDrawManager.WorldToScreen(vMon, vTop);
			
			const char* sName = playerInfo.name;
			if (gCvars.esp_box)
			gDrawManager.DrawPlayerBox(ePlayer, teamColour(teamnum));
			int y = vTop.y;
			if (gCvars.esp_name)
				gDrawManager.DrawString(true, true, vBot.x, y, teamColour(teamnum), sName);
			int hp = *(int*)((DWORD)ePlayer + netVars.m_iHealth);
			if (gCvars.esp_health)
			gDrawManager.DrawString(true, true, vBot.x, y += gDrawManager.GetESPHeight(), hpColour(hp), "%i HP", hp);
			if (gCvars.esp_dist)
				gDrawManager.DrawString(true, true, vBot.x, y += gDrawManager.GetESPHeight(), 0x3b5998FF, "%i m", (int)floor(flGetDistance(eMe->GetAbsOrigin(), ePlayer->GetAbsOrigin()) + 0.5));
		}

	}
};
extern CPTV paintTraverse;